package main

import (
	"flag"
	"log"
)

var (
	configPath = flag.String("conf", "default_config.json", "Путь до конфига")
)

func main() {
	flag.Parse()

	conf, err := readConfig(*configPath)
	if err != nil {
		log.Fatalln("Ошибка чтения конфига:", err)
	}

	db, err := conf.DB.Init()
	if err != nil {
		log.Fatalln("Ошибка создания БД:", err)
	}

	bot, err := conf.Bot.New(db)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Бот успешно авторизован под именем", bot.Username)

	if err = bot.Listen(); err != nil {
		log.Fatalln(err)
	}
}
