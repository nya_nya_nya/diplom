package main

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/nya_nya_nya/diplom/bot"
	dbp "gitlab.com/nya_nya_nya/diplom/db"
)

type config struct {
	DB  *dbp.Config
	Bot *bot.Config
}

func readConfig(path string) (*config, error) {
	fconf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	conf := new(config)
	if err = json.Unmarshal(fconf, conf); err != nil {
		return nil, err
	}

	return conf, nil
}
