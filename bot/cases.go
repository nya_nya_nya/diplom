package bot

import (
	"math/rand"
	"strconv"
	"strings"
	"time"

	"gitlab.com/nya_nya_nya/diplom/types"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func (*Bot) start(chatID int64, username string) tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(
		chatID,
		"Привет "+username+", для продолжения работы выбери интересующую кнопку",
	)
	msg.ReplyMarkup = mainKB
	return msg
}

var lastImg int

func (b *Bot) humor(chatID int64) (tgbotapi.MediaGroupConfig, error) {
	var msg tgbotapi.MediaGroupConfig
	imgs, err := b.db.GetHumor()
	if err != nil {
		return msg, err
	}

	index := rand.Intn(len(imgs))
	if index == lastImg {
		index = rand.Intn(len(imgs))
	}
	lastImg = index

	img := imgs[index]
	msg = tgbotapi.NewMediaGroup(chatID, []interface{}{
		tgbotapi.NewInputMediaPhoto(img),
	})
	return msg, nil
}

var lastFact int

func (b *Bot) fact(chatID int64) (tgbotapi.MessageConfig, error) {
	var msg tgbotapi.MessageConfig
	facts, err := b.db.GetFact()
	if err != nil {
		return msg, err
	}
	index := rand.Intn(len(facts))
	if index == lastFact {
		index = rand.Intn(len(facts))
	}
	lastFact = index

	fact := facts[index]
	msg = tgbotapi.NewMessage(chatID, fact)
	return msg, nil

}

func (b *Bot) docs(chatID int64, tp string) (tgbotapi.MessageConfig, error) {
	var msg tgbotapi.MessageConfig
	var res string
	var err error

	switch tp {
	case "Общее":
		res, err = b.db.GetCommonDocs()
	case "Криптография":
		res, err = b.db.GetCryptDocs()
	case "КТ":
		res, err = b.db.GetKTDocs()
	case "ПДн":
		res, err = b.db.GetPDnDocs()
	case "ИБ в банке":
		res, err = b.db.GetBankDocs()
	}
	if err != nil {
		return msg, err
	}
	msg = tgbotapi.NewMessage(chatID, res)
	return msg, nil
}

func (*Bot) getDocs(chatID int64) tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(
		chatID,
		"Выберите нужную категорию, для получения списка документов",
	)
	msg.ReplyMarkup = docsKB
	return msg
}

func (*Bot) question(chatID int64, username string) tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(
		chatID,
		username+", Выберите уровень сложности вопросов",
	)
	msg.ReplyMarkup = levelKB
	return msg
}

func (b *Bot) level(chatID int64, lvl string) (tgbotapi.MessageConfig, error) {
	var msg tgbotapi.MessageConfig
	var qs []types.Question
	var err error

	switch lvl {
	case "Легкий":
		qs, err = b.db.GetEasyQuestion()
	case "Средний":
		qs, err = b.db.GetNormalQuestion()
	case "Сложный":
		qs, err = b.db.GetHardQuestion()
	}
	if err != nil {
		return msg, err
	}
	if len(qs) == 0 {
		msg = tgbotapi.NewMessage(chatID, "Вопроса уровня: "+lvl+" - нет\n")
		return msg, nil
	}
	index := rand.Intn(len(qs))
	q := qs[index]

	variants := strings.Split(q.Answers, "#")
	text := genQuestionText(lvl, q)

	msg = tgbotapi.NewMessage(chatID, text)
	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.ReplyMarkup = createQuestionKB(q.ID, variants)
	return msg, nil
}

func (b *Bot) callbackQ(query *tgbotapi.CallbackQuery) (tgbotapi.EditMessageTextConfig, error) {
	res := strings.Split(query.Data, "_")

	if len(res) != 2 {
		msg := tgbotapi.NewEditMessageText(
			query.Message.Chat.ID,
			query.Message.MessageID,
			"Неправильные данные кнопки",
		)
		return msg, nil
	}
	idQ, err := strconv.Atoi(res[0])
	if err != nil {
		msg := tgbotapi.NewEditMessageText(
			query.Message.Chat.ID,
			query.Message.MessageID,
			"Неправильные данные кнопки",
		)
		return msg, nil
	}

	var msg tgbotapi.EditMessageTextConfig

	answer, err := b.db.GetAnswer(idQ)
	if err != nil {
		return msg, err
	}

	text := "------\n*Ваш ответ:*\n" + res[1]
	if res[1] == answer {
		text = text + "\n(правильный)"
		err = b.db.IncrRighAnswer(idQ)
	} else {
		text = text + "\n(неправильный)"
		err = b.db.IncrWrongAnswer(idQ)
	}
	if err != nil {
		return msg, err
	}
	q, err := b.db.GetQuestion(idQ)
	if err != nil {
		return msg, err
	}

	var lvl string
	switch q.LevelOfDifficulty {
	case 1:
		lvl = "Легкий"
	case 2:
		lvl = "Средний"
	case 3:
		lvl = "Сложный"
	}
	text = genQuestionText(lvl, q) + text

	msg = tgbotapi.NewEditMessageText(query.Message.Chat.ID, query.Message.MessageID, text)
	msg.ParseMode = tgbotapi.ModeMarkdown
	return msg, nil
}
