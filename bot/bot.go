package bot

import (
	"errors"
	"log"

	dbp "gitlab.com/nya_nya_nya/diplom/db"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

type Config struct {
	Debug bool
	Token string
}
type Bot struct {
	Username string
	db       *dbp.DB
	updates  tgbotapi.UpdatesChannel
	conn     *tgbotapi.BotAPI
}

func (c *Config) New(db *dbp.DB) (*Bot, error) {
	conn, err := tgbotapi.NewBotAPI(c.Token)
	if err != nil {
		return nil, err
	}
	conn.Debug = c.Debug

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := conn.GetUpdatesChan(u)
	if err != nil {
		return nil, err
	}

	return &Bot{
		Username: conn.Self.UserName,
		db:       db,
		updates:  updates,
		conn:     conn,
	}, nil
}

func (b *Bot) Listen() error {
	var msg tgbotapi.Chattable
	var err error
	for up := range b.updates {
		//Обработка ответа на вопрос
		if up.CallbackQuery != nil {
			msgC, errC := b.callbackQ(up.CallbackQuery)
			if errC != nil {
				return errC
			}
			if _, errSend := b.conn.Send(msgC); errSend != nil {
				log.Println("[Bot] Ошибка отправки:", errSend)
			}
			continue
		}

		if up.Message == nil { // ignore any non-Message Updates
			continue
		}

		// Обработка команды
		switch up.Message.Text {
		case "/start", "Главное меню":
			msg = b.start(up.Message.Chat.ID, up.Message.From.UserName)
		case "Вопрос":
			msg = b.question(up.Message.Chat.ID, up.Message.From.UserName)
		case "Легкий", "Средний", "Сложный":
			msg, err = b.level(up.Message.Chat.ID, up.Message.Text)
		case "Юмор":
			msg, err = b.humor(up.Message.Chat.ID)
		case "Факт":
			msg, err = b.fact(up.Message.Chat.ID)
		case "Документы":
			msg = b.getDocs(up.Message.Chat.ID)
		case "Общее", "Криптография", "КТ", "ПДн", "ИБ в банке":
			msg, err = b.docs(up.Message.Chat.ID, up.Message.Text)
		default:
			msg = b.defaulObr(up.Message.Chat.ID)
		}
		if err != nil {
			return err
		}

		if _, errSend := b.conn.Send(msg); errSend != nil {
			log.Println("[Bot] Ошибка отправки:", errSend)
		}

	}

	return errors.New("[Bot] Прекращение работы бота")
}

func (*Bot) defaulObr(chatID int64) tgbotapi.MessageConfig {
	return tgbotapi.NewMessage(chatID, "Неизвестная команда")
}
