package bot

import (
	"strconv"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

var mainKB = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Вопрос"),
		tgbotapi.NewKeyboardButton("Факт"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Юмор"),
		tgbotapi.NewKeyboardButton("Документы"),
	),
)

var mainMenuBT = tgbotapi.NewKeyboardButton("Главное меню")

var levelKB = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Легкий"),
		tgbotapi.NewKeyboardButton("Средний"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Сложный"),
		mainMenuBT,
	),
)

var docsKB = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Общее"),
		tgbotapi.NewKeyboardButton("Криптография"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("КТ"),
		tgbotapi.NewKeyboardButton("ПДн"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("ИБ в банке"),
		mainMenuBT,
	),
)

func createQuestionKB(idQ int, variants []string) tgbotapi.InlineKeyboardMarkup {
	buttons := make([][]tgbotapi.InlineKeyboardButton, 0, len(variants))

	id := strconv.Itoa(idQ)

	for _, v := range variants {
		buts := tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData(v, id+"_"+v),
		)
		buttons = append(buttons, buts)
	}
	return tgbotapi.NewInlineKeyboardMarkup(buttons...)
}
