package bot

import (
	"fmt"

	"gitlab.com/nya_nya_nya/diplom/types"
)

func genQuestionText(lvl string, q types.Question) string {
	text := "*Уровень сложности:* " + lvl + "\n\n"
	text = text + q.Question

	stat := fmt.Sprintf("\n\n*Статистика ответов:*\nПравильно: %d | Неправильно: %d\n",
		q.RightAnswers, q.WrongAnswers)
	text = text + stat
	return text
}
