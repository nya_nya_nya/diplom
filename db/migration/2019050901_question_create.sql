-- +goose Up
CREATE TABLE "question" (
	"id"						INTEGER 	PRIMARY KEY AUTOINCREMENT,
	"question"				TEXT 	NOT NULL DEFAULT '',
	"right_answers"			INTEGER	NOT NULL DEFAULT 0,
	"wrong_answers"			INTEGER	NOT NULL DEFAULT 0,
	"level_of_difficulty"	INTEGER	NOT NULL DEFAULT 0,
	"answers"				TEXT 	NOT NULL DEFAULT '',
	"correct_answer"			TEXT 	NOT NULL DEFAULT ''
);

-- +goose Down
DROP TABLE question;