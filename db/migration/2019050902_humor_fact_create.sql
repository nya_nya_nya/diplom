-- +goose Up
CREATE TABLE "humor_fact" (
	"id"			INTEGER PRIMARY KEY AUTOINCREMENT,
	"content"	TEXT 	NOT NULL DEFAULT '',
	"type"		INTEGER 	NOT NULL DEFAULT 0
);

-- +goose Down
DROP TABLE humor_fact;