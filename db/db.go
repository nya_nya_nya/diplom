package db

import (
	"database/sql"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pressly/goose"
)

type Config struct {
	Debug     bool
	Path      string
	Migration string
}
type DB struct {
	debug bool
	conn  *sqlx.DB
}

func (c *Config) Init() (*DB, error) {
	if err := goose.SetDialect("sqlite3"); err != nil {
		return nil, err
	}
	conn, err := sql.Open("sqlite3", c.Path)
	if err != nil {
		return nil, err
	}

	if err = goose.Up(conn, c.Migration); err != nil {
		return nil, err
	}

	xconn := sqlx.NewDb(conn, "sqlite3")

	return &DB{
		debug: c.Debug,
		conn:  xconn,
	}, nil
}

func (d *DB) Debug(format string, v ...interface{}) {
	if d.debug {
		log.Printf("[DB] "+format, v...)
	}
}
