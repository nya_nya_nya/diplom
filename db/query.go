package db

import (
	"gitlab.com/nya_nya_nya/diplom/types"
)

func (d *DB) getDocs(tp int) (string, error) {
	d.Debug("getDocs: tp %d", tp)

	var res string
	err := d.conn.Get(&res, "SELECT doc FROM doc WHERE type=$1", tp)
	return res, err
}

func (d *DB) GetCommonDocs() (string, error) {
	return d.getDocs(1)
}

func (d *DB) GetCryptDocs() (string, error) {
	return d.getDocs(2)
}

func (d *DB) GetKTDocs() (string, error) {
	return d.getDocs(3)
}

func (d *DB) GetPDnDocs() (string, error) {
	return d.getDocs(4)
}

func (d *DB) GetBankDocs() (string, error) {
	return d.getDocs(5)
}

func (d *DB) getHumorFact(tp int) ([]string, error) {
	d.Debug("getHumorFact: tp %d", tp)
	var res []string
	if err := d.conn.Select(&res, "SELECT content FROM humor_fact WHERE type=$1", tp); err != nil {
		return nil, err
	}
	return res, nil
}

func (d *DB) GetFact() ([]string, error) {
	return d.getHumorFact(2)
}

func (d *DB) GetHumor() ([]string, error) {
	return d.getHumorFact(1)
}

func (d *DB) getQuestion(ld int) ([]types.Question, error) {
	d.Debug("getQuestion: ld %d", ld)
	var res []types.Question
	err := d.conn.Select(&res, "SELECT * FROM question WHERE level_of_difficulty=$1", ld)
	return res, err
}

func (d *DB) GetEasyQuestion() ([]types.Question, error) {
	return d.getQuestion(1)
}

func (d *DB) GetNormalQuestion() ([]types.Question, error) {
	return d.getQuestion(2)
}

func (d *DB) GetHardQuestion() ([]types.Question, error) {
	return d.getQuestion(3)
}

func (d *DB) GetQuestion(id int) (types.Question, error) {
	d.Debug("GetQuestion: id %d", id)
	var res types.Question
	err := d.conn.Get(&res, "SELECT * FROM question WHERE id=$1", id)
	return res, err
}

func (d *DB) GetAnswer(id int) (string, error) {
	d.Debug("GetAnswer: id %d", id)
	var res string
	err := d.conn.Get(&res, "SELECT correct_answer FROM question WHERE id=$1", id)
	return res, err
}

func (d *DB) IncrRighAnswer(id int) error {
	d.Debug("IncrRighAnswer: id %d", id)
	_, err := d.conn.Exec("UPDATE question SET right_answers = right_answers + 1 WHERE id =$1", id)
	return err
}

func (d *DB) IncrWrongAnswer(id int) error {
	d.Debug("IncrWrongAnswer: id %d", id)
	_, err := d.conn.Exec("UPDATE question SET wrong_answers = wrong_answers + 1 WHERE id =$1", id)
	return err

}
