package types

type Question struct {
	ID                int    `db:"id"`
	Question          string `db:"question"`
	RightAnswers      int    `db:"right_answers"`
	WrongAnswers      int    `db:"wrong_answers"`
	LevelOfDifficulty int    `db:"level_of_difficulty"`
	Answers           string `db:"answers"`
	CorrectAnswer     string `db:"correct_answer"`
}
